package com.example.EmployeeManage.repository;

import com.example.EmployeeManage.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee,Integer> {
}
