package com.example.EmployeeManage.controller;

import com.example.EmployeeManage.exceptions.MyException;
import com.example.EmployeeManage.model.Employee;
import com.example.EmployeeManage.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.config.ConfigDataResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/employee")
public class EmployeeController {
    @Autowired
    EmployeeRepository employeeRepositoryObj;
    @GetMapping
    public List<Employee> getEmployee()
    {
        return  employeeRepositoryObj.findAll();
    }

    @PostMapping
    public Employee getEmployee(@RequestBody Employee employee)
    {
        return employeeRepositoryObj.save(employee);
    }
    @GetMapping("{id}")
    public ResponseEntity<Employee> getEmployeeById(@PathVariable int id)
    {
        Employee employee=employeeRepositoryObj.findById(id).orElseThrow(()-> new MyException("Employee does not exist: "+id));
        return ResponseEntity.ok(employee);
    }
@PutMapping("{id}")
    public ResponseEntity<Employee> updateEmployee(@PathVariable int id,@RequestBody Employee employee)
    {
        Employee updateEmployee=employeeRepositoryObj.findById(id).orElseThrow(()-> new MyException("Employee does not exist: "+id));
        updateEmployee.setEmailID(employee.getEmailID());
        updateEmployee.setFirstName(employee.getFirstName());
        updateEmployee.setLastName(employee.getLastName());
        employeeRepositoryObj.save(updateEmployee);
        return ResponseEntity.ok(updateEmployee);
    }
    @DeleteMapping("{id}")
    public void deleteEmployee(@PathVariable int id)
    {
        Employee employee=employeeRepositoryObj.findById(id).orElseThrow(()-> new MyException("Employee does not exist: "+id));
        employeeRepositoryObj.delete(employee);
        return ;
    }

}
